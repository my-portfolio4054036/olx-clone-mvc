package org.example.service;

import org.example.model.Product;
import org.example.model.User;
import org.example.repository.ProductRepository;
import org.example.repository.UserRepositary;
import org.example.utills.CommonUtills;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    UserRepositary userRepositary;

    public List<List<Product>> getAllProductsFromService(
            Integer page,
            Integer size,
            String searchWord
    ) {
        CommonUtills.validatePageAndSize(page, size);
        return productRepository.getAll(page, size, searchWord);
    }

    public List<Integer> getProductPages(String search) {
        Integer count = productRepository.getCount(search);
        List<Integer> list = new ArrayList<>();
        if (count <= 12) return list;
        else {
            for (int i = 0; i <= (count - 1) / 12; i++) {
                list.add(i + 1);
            }
        }
        return list;
    }

    public List<String> getCategory() {
        return productRepository.getCategoryList();
    }

    public User getUser(String userId) {
        return userRepositary.getUserById(userId);
    }
}
