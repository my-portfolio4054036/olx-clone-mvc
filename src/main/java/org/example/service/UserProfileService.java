package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.model.Product;
import org.example.repository.ProductRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserProfileService {
    private final ProductRepository productRepository;

    public String SubmitAd(Product product) {
        if (product.getUser_id() == null) return "user_id_false";
        productRepository.SubmitAdDb(product);
        return "";
    }
}
