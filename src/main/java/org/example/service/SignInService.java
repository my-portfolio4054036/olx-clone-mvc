package org.example.service;

import lombok.RequiredArgsConstructor;
import org.example.repository.UserRepositary;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SignInService {
    private final UserRepositary userRepositary;

    public String validateLogin(String phoneNumber, String password) {
        String word="";
        if (phoneNumber.isBlank())word+="Telefon raqam,";
        if (password.isBlank())word+="Parol";
        if (!word.isEmpty()){
            if (word.endsWith(","))word=word.substring(0,word.length()-1);
            return word + " kiritilmadi !";
        }else {
            word=userRepositary.validateUser(phoneNumber,password);
            if (!word.isBlank()) {
                return "#"+word;
            }else {
                return "Bunday User mavjud emas !";
            }
        }

    }
}
