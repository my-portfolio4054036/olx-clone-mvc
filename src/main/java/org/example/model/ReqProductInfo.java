package org.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReqProductInfo {
    private UUID id;
    private String name;
    private String price;
    private String comment;
    private String imageUrl;
    private String ownerName;
    private String ownerNumber;
}
