package org.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Product {
    private UUID id;
    private String name;
    private String comment;
    private String image_url;
    private String price;
    private Integer categoryId;
    private Integer user_id;
    private Boolean isBloced;


    public Product(UUID id, String name, String comment, String image_url, String price, Integer categoryId, Integer user_id) {
        this.id = id;
        this.name = name;
        this.comment = comment;
        this.image_url = image_url;
        this.price = price;
        this.categoryId = categoryId;
        this.user_id = user_id;
    }

    public Product(String name, String comment, String image_url, String price, Integer categoryId, Integer user_id, Boolean isBloced) {
        this.name = name;
        this.comment = comment;
        this.image_url = image_url;
        this.price = price;
        this.categoryId = categoryId;
        this.user_id = user_id;
        this.isBloced = isBloced;
    }
}
