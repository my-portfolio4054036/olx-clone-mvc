package org.example.repository;

import lombok.SneakyThrows;
import org.example.dbConfig.DatabaseManager;
import org.example.model.RoleEnum;
import org.example.model.User;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class UserRepositary {
    @SneakyThrows
    public void addUser(String fullName, String phoneNUmber, String password) {
        Connection connection = DatabaseManager.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into users (full_name, phone_number, password, role_name)" +
                    " values (?,?,?,?);");
            preparedStatement.setString(1, fullName);
            preparedStatement.setString(2, phoneNUmber);
            preparedStatement.setString(3, String.valueOf(password.hashCode()));
            preparedStatement.setString(4, RoleEnum.USER.toString());
            preparedStatement.execute();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseManager.closeConnection(connection);
    }

    @SneakyThrows
    public String validateUser(String phoneNumber, String password) {
        Connection connection = DatabaseManager.getConnection();
        String result = "";
        try {
            PreparedStatement preparedStatement = connection
                    .prepareStatement("select * from users where phone_number=? and password=?");
            preparedStatement.setString(1, phoneNumber);
            preparedStatement.setString(2, String.valueOf(password.hashCode()));
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getString("id");
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseManager.closeConnection(connection);
        return result;
    }

    @SneakyThrows
    public User getUserById(String userId) {
        if (userId == null) return null;
        User user = new User();
        Connection connection = DatabaseManager.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from users where id='" + userId + "'");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user.setId(resultSet.getInt(1));
                user.setActive(resultSet.getBoolean(2));
                user.setFullName(resultSet.getString(3));
                user.setPassword(resultSet.getString(4));
                user.setPhoneNumber(resultSet.getString(5));
                user.setRoleName(resultSet.getString(6));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseManager.closeConnection(connection);
        return user;
    }
}
