package org.example.repository;

import lombok.SneakyThrows;
import org.example.dbConfig.DatabaseManager;
import org.example.model.Product;
import org.example.model.ReqProductInfo;
import org.springframework.stereotype.Component;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@Component
public class ProductRepository {


    @SneakyThrows
    public List<List<Product>> getAll(Integer page, Integer size, String searchWord) {
        List<List<Product>> productList = new ArrayList<>();
        List<Product> list = new ArrayList<>();
        Connection connection = DatabaseManager.getConnection();
        try {
            PreparedStatement preparedStatement;
            if (searchWord == null) {
                preparedStatement = connection.prepareStatement
                        ("select cast(id as varchar) id,name,comment,image_url,price,category_id,user_id " +
                                "from product where is_bloced=false limit ? offset ?;");
            } else {
                preparedStatement = connection.prepareStatement
                        ("select cast(id as varchar) id,name,comment,image_url,price,category_id,user_id " +
                                "from product where  name like '%" + searchWord + "%' and is_bloced=false limit ? offset ?;");
            }
            preparedStatement.setInt(1, size);
            preparedStatement.setInt(2, (page * size));
            ResultSet resultSet = preparedStatement.executeQuery();
            if (searchWord == null) {
                preparedStatement = connection.prepareStatement
                        ("select count(*) from product where is_bloced=false;");
            } else {
                preparedStatement = connection.prepareStatement("select count(*) from product where  name like '%" + searchWord + "%' and is_bloced=false;");
            }
            ResultSet resultSet1 = preparedStatement.executeQuery();
            int count = 0;
            if (resultSet1.next()) {
                count = resultSet1.getInt(1);
            }
            count = count - (page * size);
            int i = 0;
            while (resultSet.next()) {
                i++;
                Product product = new Product(
                        UUID.fromString(resultSet.getString(1)),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getInt(6),
                        resultSet.getInt(7)
                );
                list.add(product);
                if (i % 4 == 0 && i != 0) {
                    productList.add(list);
                    list = new ArrayList<>();
                }
                if (i == count && !list.isEmpty()) productList.add(list);
            }
            resultSet.close();
            resultSet1.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseManager.closeConnection(connection);
        return productList;
    }


    @SneakyThrows
    public ReqProductInfo getById(String id) {
        ReqProductInfo product = new ReqProductInfo();
        Connection connection = DatabaseManager.getConnection();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from product p join users u on p.user_id = u.id where p.id='" + id + "' and p.is_bloced=false;");
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {

                product.setId(UUID.fromString(resultSet.getString(1)));
                product.setName(resultSet.getString("name"));
                product.setComment(resultSet.getString("comment"));
                product.setImageUrl(resultSet.getString("image_url"));
                product.setPrice(resultSet.getString("price"));
                product.setOwnerName(resultSet.getString("full_name"));
                product.setOwnerNumber(resultSet.getString("phone_number"));
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseManager.closeConnection(connection);
        return product;
    }

    @SneakyThrows
    public Integer getCount(String search) {
        Connection connection = DatabaseManager.getConnection();
        int count = 0;
        try {
            PreparedStatement preparedStatement;
            if (search == null) {
                preparedStatement = connection.prepareStatement
                        ("select count(*) from product ;");
            } else {
                preparedStatement = connection.prepareStatement("select count(*) from product where  name like '%" + search + "%' and is_bloced=false;");
            }
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }
            resultSet.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseManager.closeConnection(connection);
        return count;
    }

    @SneakyThrows
    public List<String> getCategoryList() {
        List<String> list = new ArrayList<>();
        Connection connection = DatabaseManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("select * from category;")) {
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(resultSet.getString(1));
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseManager.closeConnection(connection);
        return list;
    }


    @SneakyThrows
    public void SubmitAdDb(Product product) {
        Connection connection = DatabaseManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("insert into product(name, comment, image_url, price, category_id,user_id) values (?,?,?,?,?,?);")) {
            preparedStatement.setString(1, product.getName());
            preparedStatement.setString(2, product.getComment());
            preparedStatement.setString(3, product.getImage_url());
            preparedStatement.setString(4, product.getPrice());
            preparedStatement.setInt(5, 0);
            preparedStatement.setInt(6, product.getUser_id());
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseManager.closeConnection(connection);
    }

    @SneakyThrows
    public List<Product> getUsersAnnouncement(Integer userId) {
        List<Product> list = new ArrayList<>();
        if (userId == null) {
            return null;
        }
        Connection connection = DatabaseManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("select * from product where user_id=?")) {
            preparedStatement.setInt(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(new Product(
                        UUID.fromString(resultSet.getString("id")),
                        resultSet.getString("name"),
                        resultSet.getString("comment"),
                        resultSet.getString("image_url"),
                        resultSet.getString("price"),
                        resultSet.getInt("category_id"),
                        resultSet.getInt("user_id"),
                        resultSet.getBoolean("is_bloced")
                ));
            }
            resultSet.close();
            preparedStatement.close();
            DatabaseManager.closeConnection(connection);
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @SneakyThrows
    public void updateBlockedProduct(String prId, Integer user_id) {
        Connection connection = DatabaseManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("update product set is_bloced = not is_bloced where id='" + prId + "' and user_id=?;")) {
            preparedStatement.setInt(1, user_id);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseManager.closeConnection(connection);
    }

    @SneakyThrows
    public void deleteBlockedProduct(String prId, Integer user_id) {
        Connection connection = DatabaseManager.getConnection();
        try (PreparedStatement preparedStatement = connection.prepareStatement("delete from product where id='" + prId + "' and user_id=?;")) {
            preparedStatement.setInt(1, user_id);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DatabaseManager.closeConnection(connection);
    }
}
