package org.example.dbConfig;

import lombok.SneakyThrows;


import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.Connection;
import java.sql.Statement;


@WebListener
public class MyListenerGfg implements ServletContextListener {

    @SneakyThrows
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        Connection connection = DatabaseManager.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.execute("""
                    create table if not exists product(
                        id          uuid not null default gen_random_uuid(),
                        category_id integer,
                        comment     varchar(255),
                        image_url   varchar(255),
                        is_bloced  boolean default false,
                        name        varchar(255),
                        price       varchar(255),
                        user_id  integer ,
                        primary key (id)
                    );
                    """);
            statement.execute("""
                    CREATE TABLE if not exists category (
                        name varchar(255)
                    );
                    """);
            statement.execute("""
                    create table if not exists users(
                        id           serial primary key not null,
                        active       boolean default true,
                        full_name    varchar(255),
                        password     varchar(255),
                        phone_number varchar(255),
                        role_name    varchar(255)
                    );
                    """);
            statement.execute("""
                    alter table if exists product add constraint
                     fk_user_id foreign key (user_id) references users
                    """);
        } catch (Exception e) {
            e.printStackTrace();
        }
        DatabaseManager.closeConnection(connection);
    }

//    @SneakyThrows
//    @Override
//    public void contextDestroyed(ServletContextEvent servletContextEvent) {
////        DatabaseManager.closeConnection(connection);
//    }
}

