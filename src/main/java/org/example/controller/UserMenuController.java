package org.example.controller;

import lombok.RequiredArgsConstructor;
import org.example.model.Product;
import org.example.model.User;
import org.example.repository.ProductRepository;
import org.example.service.UserProfileService;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserMenuController {
    private final UserProfileService userProfileService;
    private final ProductRepository productRepository;

    @GetMapping("/profile")
    public String UserInfo(HttpSession session) {
        User user = (User) session.getAttribute("user_info");
        if (user == null) return "redirect:/";
        return "userPage";
    }

    @GetMapping("/submit_ad")
    public String getSubmitAdPage(HttpSession session) {
        User user = (User) session.getAttribute("user_info");
        if (user == null) return "redirect:/";
        return "SubmitAdPage";
    }

    @PostMapping("/submit_ad")
    public String PostSubmitPage(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String image_url,
            @RequestParam(required = false) String price,
            @RequestParam(required = false) String comment,
            HttpSession session,
            Model model
    ) {
        if (name == null || image_url == null || price == null || comment == null) {
            return "redirect:/user/profile";
        }
        User user = (User) session.getAttribute("user_info");
        String s = userProfileService.SubmitAd(
                new Product(name, comment, image_url, price, 0, user.getId(), false)
        );
        if (s.isBlank()) {
            return "userPage";
        } else {
            return "redirect:/user/profile";
        }
    }

    @GetMapping("/blocking")
    public String getBlockedPage(HttpSession session, Model model) {
        User user = (User) session.getAttribute("user_info");
        if (user == null) return "redirect:/";
        List<Product> usersProductList = productRepository.getUsersAnnouncement(user.getId());
        model.addAttribute("username", user.getFullName().toUpperCase());
        model.addAttribute("usersProductList", usersProductList);
        return "BlockedPage";
    }

    @PostMapping("/blocked_an")
    public String BlockedUsersAn(@RequestParam(required = false) UUID pr_id,
                                 Model model, HttpSession session) {
        User user = (User) session.getAttribute("user_info");
        if (user == null) return "redirect:/";
        if (pr_id == null) {
            return "redirect:/user/profile";
        }
        productRepository.updateBlockedProduct(pr_id.toString(), user.getId());
        return "redirect:/user/blocking";
    }

    @GetMapping("/delete")
    public String getDeletedPage(HttpSession session, Model model) {
        User user = (User) session.getAttribute("user_info");
        if (user == null) return "redirect:/";
        List<Product> usersProductList = productRepository.getUsersAnnouncement(user.getId());
        model.addAttribute("username", user.getFullName().toUpperCase());
        model.addAttribute("usersProductList", usersProductList);
        return "DeletedPage";
    }

    @PostMapping("/deleted_an")
    public String DeletedUsersAn(@RequestParam(required = false) UUID pr_id,
                                 Model model, HttpSession session) {
        User user = (User) session.getAttribute("user_info");
        if (user == null) return "redirect:/";
        if (pr_id == null) {
            return "redirect:/user/profile";
        }
        productRepository.deleteBlockedProduct(pr_id.toString(), user.getId());
        return "redirect:/user/delete";
    }

    @GetMapping("/exit_profile")
    public String ExitProfile(HttpSession session) {
        User user = (User) session.getAttribute("user_info");
        if (user == null) return "redirect:/";
        session.removeAttribute("user_info");
        session.removeAttribute("username");
        return "redirect:/";
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public String ExceptionHandling() {
        return "ExeptionPage";
    }


}
