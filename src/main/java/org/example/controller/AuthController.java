package org.example.controller;

import lombok.RequiredArgsConstructor;
import org.example.repository.UserRepositary;
import org.example.service.SignInService;
import org.example.utills.CommonUtills;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
    private final UserRepositary userRepositary;
    private final SignInService signInService;


    @GetMapping("/signin")
    public String SignIn() {
        return "SignInPage";
    }

    @PostMapping("/signin")
    public String SignInPost(
            @RequestParam("phone_number") String phoneNumber,
            @RequestParam("password") String password,
            Model model) {
        String s = signInService.validateLogin(phoneNumber, password);
        if (s.startsWith("#")) {
            model.addAttribute("user_id", s.substring(1));
            return "redirect:/";
        } else {
            model.addAttribute("wrong_word", s);
            return "SignInPage";
        }
    }

    @GetMapping("/signup")
    public String SignUp() {
        return "SignUpPage";
    }

    @PostMapping("/save")
    public String Register(
            @RequestParam("full_name") String fullName,
            @RequestParam("phone_number") String phoneNUmber,
            @RequestParam("password") String password,
            @RequestParam("confirm_password") String confirmPassword,
            Model model
    ) {

        String s = CommonUtills.validateUserParam(fullName, phoneNUmber, password, confirmPassword);
        if (s.equals("ok")) {
            userRepositary.addUser(fullName, phoneNUmber, password);
            return "redirect:/";
        } else {
            model.addAttribute("wrong_word", s);
            return "SignUpPage";
        }
    }

}
