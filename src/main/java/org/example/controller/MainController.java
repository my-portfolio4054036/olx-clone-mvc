package org.example.controller;

import org.example.model.Product;
import org.example.model.User;
import org.example.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
public class MainController {
    @Autowired
    ProductService productService;

    @GetMapping()
    public String getFirstPage(
            @RequestParam(required = false, defaultValue = "0")
            Integer page,
            @RequestParam(required = false, defaultValue = "12")
            Integer size,
            @RequestParam(required = false, defaultValue = "a") String next,
            @RequestParam(required = false, defaultValue = "b") String prev,
            @RequestParam(required = false) String user_id,
            Model model, HttpSession session) {
        if (page == 0 && next.equals("a") && prev.equals("b")) session.setAttribute("page", page);
        if (page != 0) {
            page = page - 1;
            session.setAttribute("page", page);
        }
        if (next.equals("next")) {
            Integer nextPage = (Integer) session.getAttribute("page");
            page = nextPage + 1;
            session.setAttribute("page", page);
        }
        if (prev.equals("prev")) {
            Integer lastPage = (Integer) session.getAttribute("page");
            page = lastPage - 1;
            session.setAttribute("page", page);
        }
        List<List<Product>> products = productService.getAllProductsFromService(page, size, null);
        List<Integer> pages = productService.getProductPages(null);
        List<String> categeoryList = productService.getCategory();
        if (pages.size() >= 2 && (page + 1) != pages.get(pages.size() - 1)) model.addAttribute("next", ">");
        if (pages.size() >= 2 && page != 0) model.addAttribute("prev", "<");
        model.addAttribute("Pages", pages);
        model.addAttribute("productList", products);
        model.addAttribute("categoryList", categeoryList);
        model.addAttribute("z", page + 1);
        User user = productService.getUser(user_id);
        if (user != null) {
            session.setAttribute("user_info", user);
        }
        String username = null;
        if (user != null) {
            username = user.getFullName();
        }
        if (username != null) {
            session.setAttribute("username", username.toUpperCase());
        }
        model.addAttribute("username", session.getAttribute("username"));
        return "indexPage";
    }

    @GetMapping("/searchList")
    public String getSearchList(
            @RequestParam(required = false, defaultValue = "0")
            Integer page,
            @RequestParam(required = false, defaultValue = "12")
            Integer size,
            @RequestParam(required = false, defaultValue = "a") String next,
            @RequestParam(required = false, defaultValue = "b") String prev,
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String search,
            Model model, HttpSession session
    ) {
        if (search != null) {
            session.setAttribute("search_word", search);
        }
        if (search == null) {
            search = (String) session.getAttribute("search_word");
        }
        if (page == 0 && next.equals("a") && prev.equals("b")) session.setAttribute("page", page);
        if (page != 0) {
            page = page - 1;
            session.setAttribute("page", page);
        }
        if (next.equals("next")) {
            Integer lastPage = (Integer) session.getAttribute("page");
            page = lastPage + 1;
            session.setAttribute("page", page);
        }
        if (prev.equals("prev")) {
            Integer lastPage = (Integer) session.getAttribute("page");
            page = lastPage - 1;
            session.setAttribute("page", page);
        }
        List<List<Product>> products = productService.getAllProductsFromService(page, size, search);
        List<Integer> pages = productService.getProductPages(search);
        List<String> categeoryList = productService.getCategory();
        if (pages.size() >= 2 && (page + 1) != pages.get(pages.size() - 1)) model.addAttribute("next", ">");
        if (pages.size() >= 2 && page != 0) model.addAttribute("prev", "<");
        model.addAttribute("Pages", pages);
        model.addAttribute("productList", products);
        model.addAttribute("categoryList", categeoryList);
        model.addAttribute("z", page + 1);
        if (username != null) {
            session.setAttribute("username", username.toUpperCase());
        }
        model.addAttribute("username", session.getAttribute("username"));
        return "SearchListPage";

    }


}
