package org.example.controller;

import org.example.model.Product;
import org.example.model.ReqProductInfo;
import org.example.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

@Controller
@RequestMapping("/product")
public class ProductController {
    @Autowired
    ProductRepository productRepository;
    @PostMapping("/info")
    public String getProducInfo(@RequestParam(required = false) UUID id, Model model){
        ReqProductInfo product=productRepository.getById(id.toString());
        model.addAttribute("product",product);
        System.out.println("product = " + product);
        return "ProductInfoPage";
    }
    @GetMapping("/search")
    public String getSearchPage(@RequestParam(required = false) String search,Model model){
        model.addAttribute("search",search);
        return "redirect:/searchList";
    }
}
