package org.example.utills;

public class CommonUtills {

    public static void validatePageAndSize(
            Integer page,
            Integer size) {

        if (page < 0) {
            throw new IllegalArgumentException("Page must be greater or equal to zero !!!");
        }
        if (size < 1) {
            throw new IllegalArgumentException("Size must be greater then zero !!!");
        }
    }

    public static String validateUserParam(String fullName, String phoneNumber, String password, String confirmPassword) {
        String wrong = "";
        if (fullName.isBlank()) wrong += " Username,";
        if (phoneNumber.isBlank()) wrong += " Telefon raqam,";
        if (password.isBlank()) wrong += " Parol,";
        if (password.isBlank()) wrong += " Qayta parol";
        if (!wrong.isEmpty()) {
            if (wrong.endsWith(",")) wrong = wrong.substring(0, wrong.length() - 1);
            wrong += " kiritilmadi !";
            return wrong;
        } else {
            if (!password.equals(confirmPassword)) {
                return "Parolni qayta kiritishda xatolik !";
            }
            if (!validatePhoneNumber(phoneNumber)) {
                return "Telefon raqam kiritishda xatolik !";
            }
        }
        return "ok";
    }

    private static boolean validatePhoneNumber(String number) {
        if (number.length() != 9) {
            return false;
        }
        for (int i = 0; i < number.length(); i++) {
            if (!Character.isDigit(number.charAt(i))) {
                return false;
            }
        }
        return true;
    }


}
